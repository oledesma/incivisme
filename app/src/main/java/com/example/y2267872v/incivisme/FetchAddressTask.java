package com.example.y2267872v.incivisme;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FetchAddressTask extends AsyncTask<Location, Void, String> {

    private final String TAG = FetchAddressTask.class.getSimpleName();
    private Context mContext;


    FetchAddressTask(Context applicationContext){
        mContext = applicationContext;
    }

    @Override
    protected String doInBackground(Location... locations) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        Location location = locations[0];
        List<Address> addresses = null;
        String resultMessage = "";
        try{
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1
            );
        }catch (IOException ioEx){
            resultMessage = "Servicio no disponible";
            Log.e(TAG, resultMessage, ioEx);
        }catch (IllegalArgumentException illegalArgumentEx){
            resultMessage = "Coordenadas no válidas";
            Log.e(TAG, resultMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " + location.getLongitude(), illegalArgumentEx);
        }

        if(addresses == null || addresses.size() == 0){
            if(resultMessage.isEmpty()){
                resultMessage = "No se ha encontrado ninguna dirección";
                Log.e(TAG, resultMessage);
            }
        }else {
            Address address = addresses.get(0);
            ArrayList<String> addressesPart = new ArrayList<>();

            for(int i = 0; i<= address.getMaxAddressLineIndex(); i++){
                addressesPart.add(address.getAddressLine(i));
            }

            resultMessage = TextUtils.join("\n", addressesPart);
        }
        return resultMessage;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

}