package com.example.y2267872v.incivisme;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class RegisterActivity extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private Button btnRegistrar, btnLogin;
    private ProgressBar progressBar;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firebaseAuth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.emailR);
        password = (EditText) findViewById(R.id.passwordR);
        btnRegistrar = (Button) findViewById(R.id.registrarR);
        btnLogin = (Button) findViewById(R.id.loginR);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarUsuario();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });
    }
    public void registrarUsuario(){

        String textEmail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if(TextUtils.isEmpty(textEmail)){
            Toast.makeText(this, "Se debe ingresasr un email", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(pass)){
            Toast.makeText(this, "Se debe ingresasr una contraseña", Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.setVisibility(ProgressBar.VISIBLE);

        firebaseAuth.createUserWithEmailAndPassword(textEmail, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            progressBar.setVisibility(ProgressBar.INVISIBLE);

                            Toast.makeText(RegisterActivity.this, "Se ha registrado con exito", Toast.LENGTH_LONG).show();

                        }else {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(RegisterActivity.this, "El usuario ya existe", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(RegisterActivity.this, "No se pudo registrar", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }

    public void loginUser(){

        String textEmail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if(TextUtils.isEmpty(textEmail)){
            Toast.makeText(this, "Se debe ingresasr un email", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(pass)){
            Toast.makeText(this, "Se debe ingresasr una contraseña", Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.setVisibility(ProgressBar.VISIBLE);

        firebaseAuth.signInWithEmailAndPassword(textEmail, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                            RegisterActivity.this.startActivity(intent);
                            Toast.makeText(RegisterActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                        }else {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            Toast.makeText(RegisterActivity.this, "No se pudo loguear", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
